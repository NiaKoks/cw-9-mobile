import React from 'react';
import { StyleSheet, Text, View ,FlatList,TouchableOpacity,Modal} from 'react-native';
import {applyMiddleware,createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {connect,Provider} from 'react-redux';
export default class App extends React.Component {
    state ={
      contacts:{
        name: '',
        email: '',
        number: '',
        img: '',
      }};
    showModalHandler = ()=>{
      this.setState({
        contacts:[
            ...this.state.contacts,{
            key: String(Math.random()),
            name: this.state.name,
            email: this.state.email,
            number: this.state.number,
            img: this.state.img
          }
        ]
      })
    }

  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <FlatList
        data={this.state.contacts}
        renderItem={({item}) =>(
            <TouchableOpacity onPress={this.props.toggleModal}>
              <View style={styles.contactCard}>
              <Image source={this.props.img}/>
              <Text>{this.props.name}</Text>
              <Text>{this.props.number}</Text>
              <Text>{this.props.email}</Text>
              </View>
            </TouchableOpacity>
        )}>
        <Modal
        animationType='fade'
        transparent={false}
        visible={this.props.toggleModal}>
          <View style={{margin:50,backgroundColor: '#fff'}}>
            <View>
              <Text>I'm MODAL</Text>
              <TouchableOpacity onPress={this.props.toggleModal}>
                <Text>Hide modal</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        </FlatList>
      </View>
    );
  }
}
// const mapStateToProps = state =>{{
//   modalVisible:state.modalVisible
// }};
// const mapDispatchToProps = dispatch =>({
//   toggleModal: () => dispatch({type:'TOGGLE_MODAL'})
// });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contactCard:{
    flex: 1
  }
});
